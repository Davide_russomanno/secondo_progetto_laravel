<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{

    public $serie = [
        [
            'id' => 1,
            'img' => '\img\the100.webp',
            'img-2' => '\img\the100-2.webp',
            'nome' => 'The 100',
            'descrizione' => 'Un gruppo di ragazzi viene mandato sulla terra ormai resa inabitabile a causa di continue esplosioni e si scopre che adesso la terra è abitabile',
            'genere' => 'Fantascienza',
            'regista' => 'Jason Rothemberg',
            'anno' => 2000,
        ],
        [
            'id' => 2,
            'img' => '\img\lucifer.jpg',
            'img-2' => '\img\lucifer-2.webp',
            'nome' => 'Lucifer',
            'descrizione' => 'Il diavolo in persona decide di andare in vacanza e la sua meta è Los Angeles dove apre un locale notturno e comincia a lavorare con la polizia come detective, dove incontra Chloe anche lei detective di cui si innamora',
            'genere' => 'Commedia drammatica',
            'regista' => 'Tom Kapinos',
            'anno' => 2016,
        ],
        [
            'id' => 3,
            'img' => '\img\stranger.png',
            'img-2' => '\img\stranger-2.jpg',
            'nome' => 'Stranger Things',
            'descrizione' => 'Una serie in cui dei ragazzini si trovano costretti a combattere contro una forza oscura',
            'genere' => 'Fantascienza',
            'regista' => 'Matt Duffer',
            'anno' => 2016,
        ],
        [
            'id' => 4,
            'img' => '\img\emily.webp',
            'img-2' => '\img\emily-2.jpg',            
            'nome' => 'Emily in Paris',
            'descrizione' => 'La serie segue Lily Collins, (L ultimo tycoon) nei panni della venticinquenne Emily Cooper, un ambiziosa direttrice marketing di Chicago che ottiene inaspettatamente il lavoro dei sogni a Parigi',
            'genere' => 'Commedia Drammatica',
            'regista' => 'Andrew Fleming',
            'anno' => 2020,
        ],
    ];

    public function home() {
        
        return view('welcome', ['serie' => $this->serie]);
    }

    public function dettagli($id){
        
        foreach($this->serie as $serieSingola){
            if ($serieSingola['id'] == $id) {
                return view('dettagli', ['dettagli' => $serieSingola]);
            }
        }  
    }

    public function categorie($genere){

        $genereVuoto = [];
        foreach($this->serie as $categoriaSerie){
            if ($categoriaSerie['genere'] == $genere) {
                $genereVuoto[] = $categoriaSerie;
            }
        }
        return view('categorie', ['categorie' => $genereVuoto]);
    }

    public function anno($anno){

        $annoVuoto = [];

        foreach($this->serie as $annoSerie){
            if($annoSerie['anno'] == $anno){
                $annoVuoto[] = $annoSerie;
            }
        }
        return view('anno', ['anno' => $annoVuoto]);
    }
}
