<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Secondo Laravel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <!-- my css -->
    <link rel="stylesheet" href="\css\style.css">
  </head>
  <body>


<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand text-uppercase" href="#"><span class="letteraLogo">M</span>ondo <span class="letteraLogo">C</span>inema</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ route('homePage') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      </ul>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>

 <h1 class="d-flex justify-content-center my-3">Serie TV</h1>

 <div class="container-fluid my-5">
    <div class="row">
        @foreach($serie as $singolaSerie)
            <div class="col-12 col-md-3">
                <div class="card" style="width: 20rem;">
                    <img src="{{ $singolaSerie['img'] }}" class="img-fluid" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{ $singolaSerie['nome'] }}</h5>
                        <p class="card-text">{{ $singolaSerie['descrizione'] }}</p>
                        <a href="{{ route('paginaGenere', ['genere' => $singolaSerie['genere']]) }}">
                            <p class="card-text">{{ $singolaSerie['genere'] }}</p>
                        </a>
                        <p class="card-text">{{ $singolaSerie['regista'] }}</p>
                        <a href="{{ route('paginaAnno', ['anno' => $singolaSerie['anno']]) }}">
                            <p class="card-text">{{ $singolaSerie['anno'] }}</p>
                        </a>
                        <a href="{{ route('paginaDettagli', ['id' => $singolaSerie['id']]) }}" class="btn btn-primary">Dettagli</a>
                    </div>
                </div>
            </div>
        @endforeach   
    </div>
 </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>