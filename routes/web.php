<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;


Route::get('/', [PublicController::class, 'home'])->name('homePage');
Route::get('/dettagli/{id}', [PublicController::class, 'dettagli'])->name('paginaDettagli');
Route::get('/categorie/{genere}', [PublicController::class, 'categorie'])->name('paginaGenere');
Route::get('/anno/{anno}', [PublicController::class, 'anno'])->name('paginaAnno');


